# Visuals


I did some css modifications in my markdown editor, therefore some styles may seem weird to you: I use quotes as captions, highlighter as doubt mark, etc. See the folder `.themes`

I recommend you to use the markdown editor **Typora**, or the online editor of gitlab. Other markdown editors may work well, but without gurantees.


# Content


Language: English

Format: markdown

images are saved in a folder `Images - <filename>` in the same directory.

All content in bulled points. Opening a new level is done with **bold**, then with <u>underscore<\u>. No more than two level are opened.

Gitignore: files with more than 50MB (ask me if you want some file that is missing). See `.gitignore.recommended`