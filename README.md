# Simple Ontology for Knowledge Bases (SOK)

Usual human readable knowledge bases, like Wikipedia, are not very machine friendly (require a lot of NLP to use into AI systems like chatbots).

On the other hand, usual ontologies and standards for semantic web are verbose and difficult for humans to edit.

The SOK syntax was created to be a middle ground: easy to be used by humans, but structured enough to be processed by machines.

# Tooling and ecosystem

[Knowledge-Base-Analytics](https://gitlab.com/lsbenitezpereira/Knowledge-Base-Analytics/-/blob/master/README.md)

# Public knowledge bases

As far as I know, the https://gitlab.com/lsbenitezpereira/engineering-knowledge-management is the only public knowledge base that follows SOK.